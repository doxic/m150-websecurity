# M150 Web Security
> Work assignment for a hardened nginx config.

Basic setup script to install and configure nginx. Includes a http_sub_module template to print SSL related nginx variables.

## Installation

* [Downloads - Git](https://git-scm.com/downloads)
* [Downloads - Oracle VM VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Downloads - Vagrant by HashiCorp](https://www.vagrantup.com/downloads)

If you are not familiar with Vagrant, feel free to setup your own environment. You are free to use virtual machines or containers. On Debian-based distributions you can use `provisioner.sh`.

## Usage example

Copy this Git repository to a local folder on your machine
```sh
git clone https://gitlab.com/doxic/m150-websecurity.git
cd m150-websecurity
```

Create and configure guest machines according to Vagrantfile.
```sh
vagrant up
```

When `vagrant up` completed, enter `http://10.0.0.10` into your browser’s address bar:

You should receive the Nginx landing page:

![](header.png)

SSH into a running Vagrant machine and give access to a shell.
```sh
vagrant ssh
```
## Work Assignment

> Harden your nginx configuration.

> Comprehend the purpose of the security measurements

- Start by a deep dive into **certificates**. How are they used, why are they chained up to a root certificate? What part are hashes and public key cryptography play?
- Continue with **Cipher Suites**. Choose one and identify the segments. Differentiate between Key Exchange, Authentication, Cipher and Message Authentication. Look at other supported algorithms, understand how the Cipher Suite belongs to a TLS protocol version.
```
TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
```
- With an understanding of **Certificates** and **Cipher Suites**, you are able to analyze the **TLS handshake** executed when establishing a connection. Use a tool like `wireshark` to capture a handshake.
- Your nginx configuration should contain some other security-related directives as well. Research their meaning, why do you include `HSTS` or `ssl_dhparam`?
- Where to go from here? How can you secure your server even more?

### Marketshare

While hardening your configuration, find a balance between usability and security. Based on your type of website, you maybe can't break accessibility for older Android versions. So you either need your servers weblogs to analyze your user base or you can get your data from example [StatCounter Global Stats](https://gs.statcounter.com/browser-market-share). Whenever you see restrictions for a cipher used in your browser, come back to this data and check how many users are still browsing the web with it. 

### SSL Configuration

There are many excellent sites available with strong SSL cipher configurations. When researching, always remember one simple truth: There are people wrong on the Internet. Don't blindly copy-paste configurations from stack overflow or medium.

Some of my favorites you can use for your config:

- [Mozilla SSL Configuration Generator](https://ssl-config.mozilla.org/)
- [NGINX Config | DigitalOcean](https://www.digitalocean.com/community/tools/nginx)
- [Cipher suites · Cloudflare SSL docs](https://developers.cloudflare.com/ssl/ssl-tls/cipher-suites)
- [GitHub - cloudflare/sslconfig: Cloudflare's Internet facing SSL configuration](https://github.com/cloudflare/sslconfig)

### Assessment

When you're confident with your configuration, put it to the test! Probably the most trusted assessment tool is [SSL Server Test (Qualys SSL Labs)](https://www.ssllabs.com/ssltest/), but it requires a public facing website. The creator of SSL Server Test - Ivan Ristić - maintains a list of [off-line testing tools](https://github.com/ssllabs/research/wiki/Assessment-Tools).

In the test environment provided you find two tools to test your configuration.

1) testssl.sh

testssl.sh is a command line tool to check a server's service for the support of TLS/SSL ciphers, protocols as well as recent cryptographic flaws.

```sh
testssl 10.0.0.10
```

2) sslyze

sslyze allows you to analyze the SSL/TLS configuration in order to detect various issues (bad certificate, weak cipher suites, Heartbleed, ROBOT, TLS 1.3 support, etc.)

```sh
sslyze 10.0.0.10
```

## Cheatsheet

### Vagrant

creates and configures guest machines
```
vagrant up
```

SSH into a running Vagrant machine and give you access to a shell
```
vagrant ssh
```

stops the running machine Vagrant is managing and destroys all resources that were created during the machine creation process
```
vagrant destroy
```

### Nginx

show version and configure options then exit
```
nginx -V
```

restart Nginx service
```
sudo systemctl restart nginx
```

test configuration and exit
```
sudo nginx -t
```

main configuration file
```
/etc/nginx/nginx.conf
```

virtual server configuration 
```
/etc/nginx/conf.d/app.conf
/vagrant/app.conf
```

log files
```
/var/log/nginx/access.log
/var/log/nginx/error.log
```

## Release History

* 0.1.2
    * Spell check, typos fixed
    * CHANGE: Adding git as requirements
    * CHANGE: Clarifying work assignments
* 0.1.1
    * nginx 101: Adding some basic commands and paths
    * CHANGE: Adding cheatsheet section to README
* 0.1.0
    * Good enough for a first run
    * CHANGE: Adding work assignments to README
    * CHANGE: Adding assessment tools to provisioner
* 0.0.2
    * The first working release
    * ADD: README
    * CHANGE: Outsourced provisioner in Vagrantfile
    * ADD: Nginx config and template
* 0.0.1
    * Initial release
    * Work in progress

## Meta

Dominic Ruettimann – [@doxic](https://twitter.com/doxic) – dominic.ruettimann@gmail.com

Distributed under the CC-BY-SA-4.0 license. See ``LICENSE`` for more information.

[https://gitlab.com/doxic](https://gitlab.com/doxic)

## Contributing
1. Fork it (<https://gitlab.com/doxic/m150-websecurity/-/forks/new>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request