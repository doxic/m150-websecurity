#!/bin/bash

# ------------------------------------------------------------------
# [Dominic Ruettimann] M150 - Web Security Provisioner
# Basic setup scritp to install and configure nginx.
# ------------------------------------------------------------------

set -o errexit   # Exit on error. Append '||true' if you expect an error
set -o nounset   # Exit on empty variable
set -o pipefail  # Bash will remember & return the highest exitcode in a chain of pipes

# First, update local package index so we have access to most recent package listings
sudo apt-get update

# Install testssl
if [ $(dpkg-query -W -f='${Status}' testssl.sh 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  sudo apt-get install --yes testssl.sh
fi

# Install python3 and PIP
if [ $(dpkg-query -W -f='${Status}' python3-pip 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  sudo apt-get install --yes python3-pip
fi

# Install sslyze
if ! pip3 list | grep -F sslyze &> /dev/null; then
    # Never use sudo to install pip packages. Here be dragon
    sudo pip3 install sslyze
fi

# Install vim
if [ $(dpkg-query -W -f='${Status}' vim 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  sudo apt-get install --yes vim
fi

# Install nginx
if [ $(dpkg-query -W -f='${Status}' nginx 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  sudo apt-get install --yes nginx
fi

# Check if default nginx config is linked in sites-enabled
NGINX_DEFAULT="/etc/nginx/sites-enabled/default"
if [ -L ${NGINX_DEFAULT} ] && [ -e ${NGINX_DEFAULT} ]; then
    sudo rm ${NGINX_DEFAULT}
fi

# Symlink configuration files from this repository inside nginx folder
NGINX_APP="/etc/nginx/conf.d/app.conf"
NGINX_INDEX="/var/www/html/index.html"

if [ ! -e ${NGINX_APP} ]; then
    sudo ln --symbolic /vagrant/app.conf ${NGINX_APP}
fi

if [ ! -e ${NGINX_INDEX} ]; then
    sudo ln --symbolic /vagrant/index.html ${NGINX_INDEX}
fi

# Create new private key and self-signed certificate
if [ ! -f /etc/ssl/private/app.key ] || [ ! -f /etc/ssl/certs/app.pem ] ; then
    # Because of the process substitution, we cannot run this command with sudo - we will need a root shell
    sudo su -c 'openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout /etc/ssl/private/app.key \
    -new \
    -out /etc/ssl/certs/app.pem \
    -subj /CN=app_server \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /etc/ssl/openssl.cnf \
        <(printf "[SAN]\nsubjectAltName=DNS:app_server,IP:10.0.0.10")) \
    -sha256 \
    -days 3650 2>/dev/null'
fi

# Restart nginx to load new configuration
sudo systemctl restart nginx